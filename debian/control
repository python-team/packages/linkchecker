Source: linkchecker
Section: web
Priority: optional
Maintainer: Antoine Beaupré <anarcat@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               gettext,
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-hatchling,
               python3-hatch-vcs,
               python3-tomli,
               python3-packaging,
               python3-pip,
               python3-pyparsing,
               python3-requests,
               python3-typing-extensions,
               python3-urllib3,
Standards-Version: 4.1.5
Homepage: https://linkcheck.github.io/linkchecker/
Vcs-Git: https://salsa.debian.org/python-team/packages/linkchecker.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/linkchecker

Package: linkchecker
Architecture: any
Depends: python3,
         python3-bs4 (>= 4.8.1),
         python3-requests,
         python3-urllib3,
         python3-dnspython (>= 2.0),
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: ${python3:Provides}
Suggests: clamav-daemon,
          linkchecker-web,
          python3-argcomplete,
          python3-cssutils,
          python3-gconf,
          python3-geoip,
          python3-meliae
Description: check websites and HTML documents for broken links
 Provides a command line program and web interface to check links
 of websites and HTML documents.
 Features:
  * recursive checking
  * multithreaded
  * output in colored or normal text, HTML, SQL, CSV, XML or a sitemap
    graph in different formats
  * HTTP/1.1, HTTPS, FTP, mailto:, news:, nntp:, Telnet and local file
    links support
  * restrict link checking with regular expression filters for URLs
  * proxy support
  * username/password authorization for HTTP, FTP and Telnet
  * robots.txt exclusion protocol support
  * Cookie support
  * i18n support
  * HTML and CSS syntax check
  * Antivirus check

Package: linkchecker-web
Architecture: all
Depends: linkchecker,
         python3,
         ${misc:Depends},
         ${python3:Depends}
Recommends: apache2 | httpd,
            libapache2-mod-wsgi-py3 | httpd-wsgi
Description: check websites and HTML documents for broken links (web client)
 Provides a web interface to check links of websites and HTML documents.
 Features:
  * recursive checking
  * multithreaded
  * HTTP/1.1, HTTPS, FTP, mailto:, news:, nntp:, Telnet and local file
    links support
  * proxy support
  * username/password authorization for HTTP, FTP and Telnet
  * robots.txt exclusion protocol support
  * i18n support
